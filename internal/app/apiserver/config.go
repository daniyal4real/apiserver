package apiserver

import "rest/internal/app/storage"

type Config struct {
	BindAddress string `toml:"bind_address"`
	LogLevel    string `toml:"log_level"`
	Store       *storage.Config
}

func NewConfig() *Config {
	return &Config{
		BindAddress: ":9090",
		LogLevel:    "debug",
		Store:       storage.NewConfig(),
	}
}
