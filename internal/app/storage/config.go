package storage

type Config struct {
	DatabaseURL string `toml:"database_url"`
}

func NewConfig() *Config {
	return &Config{
		DatabaseURL: "host=localhost user=postgres password=2747 dbname=restapi sslmode=disable",
	}
}
