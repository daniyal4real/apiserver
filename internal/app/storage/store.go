package storage

import (
	"database/sql"
	_ "github.com/lib/pq"
)

type Store struct {
	config         *Config
	DB             *sql.DB
	userRepository *UserRepository
}

func New(config *Config) *Store {
	return &Store{
		config: config,
	}
}

func (s *Store) Open() error {

	db, err := sql.Open("postgres", s.config.DatabaseURL)
	if err != nil {
		return err
	}

	if err := db.Ping(); err != nil {
		return err
	}
	s.DB = db

	return nil
}

func (s *Store) Close() {
	s.DB.Close()
}

func (s *Store) User() *UserRepository {
	if s.userRepository != nil {
		return s.userRepository
	}

	s.userRepository = &UserRepository{
		Store: s,
	}

	return s.userRepository
}
