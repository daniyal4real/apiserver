package main

import (
	"flag"
	"github.com/BurntSushi/toml"
	"log"
	"rest/internal/app/apiserver"
)

var (
	configPath string
)

func init() {
	//value := "configs/apiserver.toml"
	flag.StringVar(&configPath, "config-path", "", "path to config file")
}

func main() {
	flag.Parse()

	config := apiserver.NewConfig()
	_, err := toml.Decode(configPath, config)
	if err != nil {
		log.Fatal(err)
	}
	server := apiserver.New(config)

	if err := server.Start(); err != nil {
		log.Fatal(err)
	}
}
